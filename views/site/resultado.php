<?php

use yii\grind\GrindView;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
    <p class="lead"><?= $enunciado ?></p>
    <div class="well">
        <?= $sql?>
    </div>
</div>
    <?= GrindView::widget([
        'dataProvider' => $resultado,
        'colums' => $campos
            
    ]); ?>
